## Feature request

Please have a quick check through our [open feature request issues](https://gitlab.com/JOGL/JOGL/-/issues?label_name[]=Feature&label_name[]=request) if your feature has already been requested.
If so, you can click on :thumbsup: to indicate that you also want this feature; and you can add a comment to the issue give more detail. If not, go ahead and create this new issue.

### Describe your feature request

A clear and concise description of what you want and what your use case is.

### Describe the solution you'd like

A clear and concise description of what you want to happen.

### Describe alternatives you've considered

A clear and concise description of any alternative solutions or features you've considered.

/label ~Feature ~request
