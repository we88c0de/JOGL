# JOGL Kubernetes Deployment

# Concepts

The scripts here deal with three main concepts to deploy the JOGL platform: Projects, Clusters and Stacks.

## Projects

The scripts here run in the context of a Google Cloud Project to which you needs to be initialized before hand through the `init-project` script. This script can be run on a newly created project. The user running these scripts needs to have the `add-iam-policy-binding` permission for both projects and service-accounts.

## Clusters

A single Google Cloud Project may contain several kubernetes clusters for different usages, for instance a cluster for development and one for staging.  Clusters may be created and destroyed through the `create-cluster` and `destroy-cluster` scripts.  The cluster comes with a Postgres Cloud SQL instance on a private ip address.

⚠️ Destroying a cluster with the `destroy-cluster` script *destroys all resources associated with it including the associated Postgres instance* so be careful and make a dump of your data if you need to.

## Stacks

The inspiration for a `STACK` comes from the pulumi framework which [defines a stack](https://www.pulumi.com/docs/intro/concepts/stack/) as:

> Every Pulumi program is deployed to a stack. A stack is an isolated, independently configurable instance of a Pulumi program. Stacks are commonly used to denote different phases of development (such as development, staging and production) or feature branches (such as feature-x-dev, jane-feature-x-dev).

Here a `STACK` effectively groups together the kubernetes resources needed to run a particular version of the jogl framework:

- a redis service
- the jogl frontend
- the jogl backend
- an ingress with https endpoints for the frontend and api

## Lifecycle

![Lifecycle](./images/lifecycle.png)

The above diagram illustrates how the different commands work together.  On a brand new Google Cloud project, the `init-project` command sets up the right permissions and resources global to the project.

Then it's possible to setup a kubernetes cluster and associated Postgres Cloud SQL DB using the `create-cluster` command.

Finally, to deploy a specific version of the JOGL platform, the `create-stack` command is used.  To redeploy a new version, it's currently recommended to destroy the stack using `destroy-stack` and recreate a new one using `create-stack`.

The `destroy-cluster` command may be useful when running tests or moving a JOGL platform instance to another cluster.

# Prerequisites

## Install the Google Cloud SDK

See the official instructions to install the Google Cloud SDK:

https://cloud.google.com/sdk/docs/downloads-interactive

A static ip address is used globally for all stacks and needs to be created manually beforehand, until the current codebase can manipulate DNS settings for the chosen domain:

`gcloud compute addresses create jogl-dev-ip --region europe-west4`

Traffic is routed to different stacks using hostname based routing, through the ingress service host rules.

## Install kubectl

`kubectl` is the kubernetes command line tool and is required to create and destroy resources in a kubernetes cluster.  You can install `kubectl` for your operating system by following the instructions below:

https://kubernetes.io/docs/tasks/tools/install-kubectl/

## Install yq

`yq` is used to customize certain manifest files on the fly.  See https://github.com/mikefarah/yq for more information on `yq` and https://learnk8s.io/templating-yaml-with-code on different approaches to customizing manifest files including `yq`.

```bash
sudo add-apt-repository ppa:rmescandon/yq
sudo apt-get install yq
```

## Roles

In order to be able to create the peering between the VPC and Cloud SQL instance you need your administrator to grant you the Compute Network Admin role as follows, for instance for `luis@jogl.io`:

```
gcloud projects add-iam-policy-binding jogl-dev --member=user:luis@jogl.io --role=roles/compute.networkAdmin
```

## Dotenv files

### Cluster Dotenv files

In order to create or destroy a cluster with the `create-cluster` and `destroy-cluster` scripts, the cluster name that is passed as a parameter should correspond to a cluster dotenv file (or the environment variables therein need to have been set).  For instance for the cluster named `jogl-dev` the scripts will look for a `jogl-dev.cluster.env` file in the `k8s` directory of this repository.

A cluster dotenv file needs to have the following values, for instance for a small, low-cost development cluster:

```bash
# Google Cloud deployment parameters
GCLOUD_PROJECT=jogl-k8s-test
GCLOUD_REGION=europe-west4

# Kubernetes deployment parametersd
GKE_MACHINE_TYPE=g1-small

# Database credentials
DATABASE_INSTANCE=jogl-dev
DATABASE_ROOT_PASS=204b603a-b7ba-11ea-af35-377cb12a7f24
DATABASE_STORAGE_SIZE=10G
DATABASE_TIER=db-f1-micro
```

A sample cluster dotenv file is provided here:

[sample.cluster.env](./sample.cluster.env)

### Stack Dotenv files

In order to create or destroy a stack with the `create-stack` and `destroy-stack` scripts, the cluster name that is passed as a parameter should correspond to a stack dotenv file (or the environment variables therein need to have been set).  For instance for the stack named `jogl-dev` the scripts will look for a `jogl-dev.env` file in the top-level directory of this repository.

A stack dotenv file needs to have the following values:


```bash
#
# Cluster Settings
#

# Google Cloud deployment parameters
GCLOUD_PROJECT=jogl-k8s-test
GCLOUD_REGION=europe-west4

# Database credentials
DATABASE_INSTANCE=jogl-dev

#
# Stack Settings
#

# Algolia credentials
ALGOLIA_API_TOKEN=
ALGOLIA_APP_ID=

# AWS credentials
AWS_S3_KEY_ID=
AWS_S3_SECRET=

# JOGL urls
BACKEND_URL=http://api.jogl.tech
FRONTEND_URL=http://app.jogl.tech

# JOGL Frontend
REACT_APP_ADDRESS_BACK=http://api.jogl.tech
REACT_APP_ALGOLIA_APP_ID=
REACT_APP_ALGOLIA_TOKEN=
REACT_APP_NODE_ENV=production

# Database credentials
DATABASE_NAME=jogl-dev
DATABASE_PASS=<password>
DATABASE_USER=<user>

# Elastic credentials
ELASTIC_APM_SECRET_TOKEN=
ELASTIC_APM_SERVER_URL=

# Logstash
LOGSTASH_PORT=5044
LOGSTASH_SERVER=51.158.70.194

# Mailchimp credentials
MAILCHIMP_API_KEY=
MAILCHIMP_LIST_ID=

# Rails
RAILS_ENV=production
RAILS_LOG_TO_STDOUT=TRUE
WEB_CONCURRENCY=2

# Redis
REDIS_URL=redis://redis:6379

# ???
SECRET_KEY_BASE=

# Smtp server settings
JOGL_EMAIL=bot@jogl.io
SMTP_DOMAIN=jogl.io
SMTP_PASSWORD=
SMTP_PORT=587
SMTP_URL=mail.gandi.net
SMTP_USER=bot@jogl.io
```
A sample stack dotenv file is provided here:

[sample.env](./sample.env)


# Scripts

The following scripts are located in the `k8s/scripts` directory of this repo.

## Initialize a Google Cloud Project

The `init-project` script will initialize a Google Cloud Project by enabling the necessary apis, creating a google service account for the project and giving it the necessary roles and creating a static ip address that may be used as an endpoint for ingress rules for the stacks in this project.  Note that the user of this script must have at least Editor or Owner rights to the given project.

```bash
k8s/scripts/init-project <project-name>
```

## Provision Google Cloud Infrastructure

The `create-cluster` script will create gcloud infrastructure (a gke cluster and database) for any stacks that may be deployed:

```bash
k8s/scripts/create-cluster <cluster-name>
```

## Stack Deployment

The `scripts/create-stack` script will deploy the JOGL platform onto the newly created cluster:

```bash
k8s/scripts/create-stack <stack-name>
```

It's perfectly possible to deploy several stacks on the same cluster, for instance a stack corresponding to a main development branch, and others corresponding to feature or bug fix branches.

## Stack Destruction

The `scripts/destroy-stack` script will destroy the JOGL stack:

```bash
k8s/scripts/destroy-stack <stack-name>
```

Following the sucessful execution of this command, the following endpoints will become available after a couple of minutes (essentially the time needed to bring the different deployments on line and to create the necessary SSL certificates).

- https://app.\<stack-name\>.jogl.tech
- https://api.\<stack-name\>.jogl.tech

## Cluster Destruction

The `scripts/destroy-cluster` script will destroy the google cloud infrastructure for the stack including the GKE Cluster and Postgres Cloud SQL instance:

```bash
k8s/scripts/destroy-cluster <cluster-name>
```
